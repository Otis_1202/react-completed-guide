import React, { useEffect, useRef, useContext } from 'react'
import PropTypes from 'prop-types'
import AuthContext from '../../context/auth-context'
const Cockpit = (props) => {
    
    const toggleBtnRef = useRef(null)
    const authContext = useContext(AuthContext)
    
    /***
     * useEffect method is a combination of componentDidMount and componentDidUpdate
     * If the second argument is an empty array, this method will excute one time and never run again. It is a way to differentiate between componentDidMount and componentDidUpdate
     * The return is not require. It runs BEFORE the main useEffect function runs, but AFTER the (first) render cycle
     * */
    useEffect(() => {
        console.log('Cockpit.js useEffect')
        toggleBtnRef.current.click()
        // Make http request
        setTimeout(() => {
            console.log('Save data to cloud!')
        }, 1000)
    }, [props.personsLength])

    useEffect(() => {
        return () => {
            console.log('Cockpit.js destroyed')
        }
    }, [props.showCockpit])

    return (
        <div>
            <h1>Hello React</h1>
            <p>Number of user: {props.personsLength}</p>
            <button ref={toggleBtnRef} onClick={props.switchNamehandler}>Switch Name</button>
            {<button onClick={authContext.login}>Log in</button>}
        </div>
    )
}

Cockpit.propTypes = {
    personsLength: PropTypes.number,
    showCockpit: PropTypes.bool,
    switchNamehandler: PropTypes.func
}

export default React.memo(Cockpit)