import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Person from './Person/Person'

class Persons extends PureComponent {

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('Persons.js shouldComponentUpdate')
  //   console.log(nextProps)
  //   console.log(nextState)
  //   // must return boolean
  //   if(nextProps.persons !== this.props.persons) {
  //     return true
  //   }
  //   return false;
  // }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('Persons.js getSnapshotBeforeUpdate')
    console.log(prevProps)
    console.log(prevState)

    // must return something
    return {message: 'Snapshot!!!'}
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('Persons.js componentDidUpdate')
    console.log(snapshot)
  }

  render() {
    console.log('Persons.js rendering...')

    return this.props.persons.map((person) => {

      return <Person key = {
        Math.random() * 1000
      }
      info = {
        {
          name: person.name,
          age: person.age
        }
      }
      />
    })
  }
  
}

Persons.propTypes = {
  persons: PropTypes.array
}

export default Persons