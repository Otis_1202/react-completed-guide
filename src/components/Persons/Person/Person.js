import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Aux from '../../../hoc/Auxilliary'
import withClass from '../../../hoc/WithClass2'
import AuthContext from '../../../context/auth-context'

class Person extends Component {
    
    constructor(props) {
        super(props)
        this.nameRef = React.createRef()
    }

    static contextType = AuthContext

    componentDidMount() {
        console.log(this.nameRef.current)
        console.log(this.context.authenticated)
        console.log(this.context.login)
    }

    render() {
        console.log('Person.js is rendering...')
        let info = this.props.info
        return (
            /**
             * Consumer Context takes a function as a child, not takes JSX
             */
            <Aux>
                {this.context.authenticated ? <p>Authenticated</p> : <p>Please login</p>}
                <p ref={this.nameRef}>Name: {info.name}</p>
                <p>Age: {info.age}</p>
                <p>{this.props.children}</p>
                <p>---------</p>
            </Aux>
        )
    }
    
}

Person.propTypes = {
    info: PropTypes.shape({
        name: PropTypes.string,
        age: PropTypes.number
    })
}

export default withClass(Person)