import React from 'react'

/**
 * The first way to declare a high order component
 * WithClass is a component, this is the same with React.Fragment 
 */
const WithClass = props => (
    <div className={props.classes}>
        {props.children}
    </div>
)

export default WithClass