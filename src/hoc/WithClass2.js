import React from 'react'

/**
 * The second way to declare a high order component
 * withClass is a javascript function
 */

const withClass = (WrappedComponent, className=null) => {
    return props => (
        <div className={className}>
            <WrappedComponent {...props}></WrappedComponent>
        </div>
    )
}

export default withClass