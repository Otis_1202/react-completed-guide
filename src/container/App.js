import { useState } from 'react';
import './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';

const App = () => {
  const [personState, setPersonState] = useState({
    persons: [
      {name: 'Phuc', age: 22},
      {name: 'Otis', age: 20}
    ]
  })

  const switchNamehandler = () => {
    console.log('Was clicked!');
    let item = personState.persons[0]
    setPersonState({
      persons: [
        {
          ...item,
          name: item.name === 'Duc Phuc' ? 'Phuc' : 'Duc Phuc'
        },
        ...personState.persons.slice(1,)
      ]
    })
  }

  return (
    <div className="App">
      <Cockpit switchNamehandler={switchNamehandler} />
      <Persons persons={personState.persons} />
    </div>
  );
}

export default App;
