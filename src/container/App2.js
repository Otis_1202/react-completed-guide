import React, {Component} from 'react'
import './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
// import WithClass from '../hoc/WithClass'
import Aux from '../hoc/Auxilliary'
import withClass from '../hoc/WithClass2'
import AuthContext from '../context/auth-context'

class App2 extends Component {

    constructor(props) {
        super(props)
        console.log('App.js constructor')
    }

    state = {
        persons: [
            {name: 'Phuc', age: 22},
            {name: 'Otis', age: 20}
        ],
        showCockpit: true,
        changeCounter: 0,
        authenticated: false
    }

    static getDerivedStateFromProps(props, state) {
        console.log('App js getDrivedStateFromProps', props)
        return state
    }
    
    componentDidMount() {
        console.log('App.js componentDidMount')
    }

    shouldComponentUpdate() {
        console.log('App.js shouldComponentUpdate')
        return true
    }

    componentDidUpdate() {
        console.log('App.js compenentDidUpdate')
    }

    switchNamehandler() {
        console.log('Was clicked!');
        this.setState((prevState, props) => {
            let item = prevState.persons[0]
            let personsState = prevState.persons
            return {
                persons: [
                    {
                    ...item,
                    name: item.name === 'Duc Phuc' ? 'Phuc' : 'Duc Phuc'
                    },
                    ...personsState.slice(1,)
                ],
                changeCounter: prevState.changeCounter + 1
            }
        })
    }

    toggleCockpit() {
        console.log("run")
        this.setState({
            showCockpit: !this.state.showCockpit
        })
    }

    loginHandler = () => {
        this.setState({
            authenticated: true
        })
    }

    render() {
        console.log('App js render')
        return (
            <Aux>
                <button onClick={this.toggleCockpit.bind(this)}>Toggle Cockpit</button>
                <AuthContext.Provider value={{authenticated: this.state.authenticated, login: this.loginHandler}}>
                    {this.state.showCockpit ?
                    <Cockpit switchNamehandler={this.switchNamehandler.bind(this)} personsLength={this.state.persons.length} showCockpit={this.state.showCockpit} />
                    : null }
                    <Persons persons={this.state.persons} />
                </AuthContext.Provider>
            </Aux>
        )
    }
}

export default withClass(App2, 'App2')